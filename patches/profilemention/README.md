## DisTok CutTheCord: Profile Mention Patch

This patch restores the behavior where tapping a user's profile picture adds their mention to the chat box instead of opening their profile.

#### Available and tested on:
- 22.4
- 22.5
- 23.0
- 24-alpha2
- 24
- 28-alpha2
- 28.1
- 29-alpha1
- 30.0
- 30.1
- 31-alpha1
- 31-alpha2
- 32-alpha2
- 32.0
- 33.1

